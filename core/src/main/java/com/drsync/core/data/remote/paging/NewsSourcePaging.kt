package com.drsync.core.data.remote.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.drsync.core.data.remote.network.ApiService
import com.drsync.core.data.remote.response.Article
import javax.inject.Inject

class NewsSourcePaging @Inject constructor(
    private val apiService: ApiService,
    private val source: String
): PagingSource<Int, Article>() {

    companion object{
        const val INITIAL_PAGE_INDEX = 1
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Article> {
        return try {
            val position = params.key ?: INITIAL_PAGE_INDEX
            val responseData = apiService.getArticleBySource(source, position, params.loadSize).articles

            LoadResult.Page(
                data = responseData as List<Article>,
                prevKey =  if (position == INITIAL_PAGE_INDEX) null else position - 1,
                nextKey = if (responseData.isEmpty()) null else position + 1
            )
        } catch (e: Exception) {
            return LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Article>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }
}