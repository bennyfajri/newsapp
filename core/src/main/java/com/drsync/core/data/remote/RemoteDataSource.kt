package com.drsync.core.data.remote

import android.util.Log
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.drsync.core.data.Result
import com.drsync.core.data.remote.network.ApiService
import com.drsync.core.data.remote.paging.NewsPaging
import com.drsync.core.data.remote.paging.NewsSourcePaging
import com.drsync.core.data.remote.paging.TopNewsPaging
import com.drsync.core.data.remote.response.Article
import com.drsync.core.data.remote.response.ServerResponse
import com.drsync.core.util.ConstantVariable.STATUS_OK
import com.drsync.core.util.ConstantVariable.TAG
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class RemoteDataSource @Inject constructor(
    private val apiService: ApiService
) {
    fun getSources(category: String) = flow<Result<ServerResponse>> {
        emit(Result.Loading())
        val response = apiService.getSources(category)
        response.let {
            if (it.status == STATUS_OK) emit(Result.Success(it))
            else emit(Result.Error(it.status))
        }
    }.catch {
        Log.d(TAG, "getSources: ${it.message}")
        emit(Result.Error(it.message.toString()))
    }

    fun getNewsBySource(source: String): Flow<PagingData<Article>> {
        return Pager(
            config = PagingConfig(pageSize = 10),
            pagingSourceFactory = {
                NewsSourcePaging(apiService, source)
            }
        ).flow
    }

    fun getNews(query: String? = null): Flow<PagingData<Article>> {
        return Pager(
            config = PagingConfig(pageSize = 10),
            pagingSourceFactory = {
                NewsPaging(apiService, query)
            }
        ).flow
    }

    fun getTopNews(country: String): Flow<PagingData<Article>> {
        return Pager(
            config = PagingConfig(pageSize = 10),
            pagingSourceFactory = {
                TopNewsPaging(apiService, country)
            }
        ).flow
    }
}