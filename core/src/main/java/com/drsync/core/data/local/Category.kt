package com.drsync.core.data.local

data class Category(
    var name: String? = "",
    var icon: Int? = 0,
    var backgroundColor: String? = ""
)
