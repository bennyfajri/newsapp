package com.drsync.core.data.remote.network

import com.drsync.core.BuildConfig
import com.drsync.core.data.remote.response.ServerResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("top-headlines/sources")
    suspend fun getSources(
        @Query("category") category: String,
        @Query("apiKey") apiKey: String = BuildConfig.API_KEY
    ) : ServerResponse

    @GET("top-headlines")
    suspend fun getArticleBySource(
        @Query("sources") sources: String,
        @Query("page") page: Int,
        @Query("pageSize") size: Int,
        @Query("apiKey") apiKey: String = BuildConfig.API_KEY
    ) : ServerResponse

    @GET("everything")
    suspend fun getArticles(
        @Query("q") query: String? = null,
        @Query("page") page: Int,
        @Query("pageSize") size: Int,
        @Query("apiKey") apiKey: String = BuildConfig.API_KEY
    ) : ServerResponse

    @GET("top-headlines")
    suspend fun getTopNews(
        @Query("country") country: String,
        @Query("page") page: Int,
        @Query("pageSize") size: Int,
        @Query("apiKey") apiKey: String = BuildConfig.API_KEY
    ) : ServerResponse

}