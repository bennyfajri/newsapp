package com.drsync.core.data.remote.response

data class ServerResponse(
    val status: String,
    val sources: List<Sources>? = null,
    val totalResults: Int? = null,
    val articles: List<Article>? = null
)