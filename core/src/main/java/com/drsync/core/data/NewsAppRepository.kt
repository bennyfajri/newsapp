package com.drsync.core.data

import com.drsync.core.data.remote.RemoteDataSource
import javax.inject.Inject

class NewsAppRepository @Inject constructor(
    private val remoteData: RemoteDataSource
) {
    fun getSource(category: String) = remoteData.getSources(category)

    fun getNewsBySource(source: String) = remoteData.getNewsBySource(source)

    fun getNews(query: String? = null) = remoteData.getNews(query)

    fun getTopNews(country: String) = remoteData.getTopNews(country)
}