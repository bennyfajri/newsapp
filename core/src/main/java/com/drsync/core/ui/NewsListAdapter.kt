package com.drsync.core.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.drsync.core.data.remote.response.Article
import com.drsync.core.databinding.ItemNewsBinding

class NewsListAdapter(
    private val onClick: (String) -> Unit
) : PagingDataAdapter<Article, NewsListAdapter.ViewHolder>(DIFF_CALLBACK){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemNewsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = getItem(position)
        data?.let { holder.bind(it) }
    }

    inner class ViewHolder(
        private val binding: ItemNewsBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Article){
            binding.apply {
                tvAuthor.text = data.author
                tvNewsTitle.text = data.title
                tvNewsDesc.text = data.description
                Glide.with(itemView.context)
                    .load(data.urlToImage)
                    .error(android.R.color.darker_gray)
                    .into(icNews)
                itemView.setOnClickListener {
                    onClick(data.url)
                }
            }
        }
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Article>() {
            override fun areItemsTheSame(oldItem: Article, newItem: Article): Boolean {
                return  oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Article, newItem: Article): Boolean {
                return oldItem.url == newItem.url
            }
        }
    }
}