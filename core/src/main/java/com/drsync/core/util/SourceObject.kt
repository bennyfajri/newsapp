package com.drsync.core.util

import com.drsync.core.data.remote.response.Sources

object SourceObject {
    val listSource= listOf(
        Sources(
            "argaam",
            "Argaam",
            "ارقام موقع متخصص في متابعة سوق الأسهم السعودي تداول - تاسي - مع تغطيه معمقة لشركات واسعار ومنتجات البتروكيماويات , تقارير مالية الاكتتابات الجديده ",
            "http://www.argaam.com",
            "business",
            "ar",
            "sa"
        ),
        Sources(
            "australian-financial-review",
            "Australian Financial Review",
            "The Australian Financial Review reports the latest news from business, finance, investment and politics, updated in real time. It has a reputation for independent, award-winning journalism and is essential reading for the business and investor community.",
            "http://www.afr.com",
            "business",
            "en",
            "au"
        ),
        Sources(
            "argaam",
            "Argaam",
            "ارقام موقع متخصص في متابعة سوق الأسهم السعودي تداول - تاسي - مع تغطيه معمقة لشركات واسعار ومنتجات البتروكيماويات , تقارير مالية الاكتتابات الجديده ",
            "http://www.argaam.com",
            "business",
            "ar",
            "sa"
        ),
        Sources(
            "australian-financial-review",
            "Australian Financial Review",
            "The Australian Financial Review reports the latest news from business, finance, investment and politics, updated in real time. It has a reputation for independent, award-winning journalism and is essential reading for the business and investor community.",
            "http://www.afr.com",
            "business",
            "en",
            "au"
        ),
        Sources(
            "argaam",
            "Argaam",
            "ارقام موقع متخصص في متابعة سوق الأسهم السعودي تداول - تاسي - مع تغطيه معمقة لشركات واسعار ومنتجات البتروكيماويات , تقارير مالية الاكتتابات الجديده ",
            "http://www.argaam.com",
            "business",
            "ar",
            "sa"
        ),
        Sources(
            "australian-financial-review",
            "Australian Financial Review",
            "The Australian Financial Review reports the latest news from business, finance, investment and politics, updated in real time. It has a reputation for independent, award-winning journalism and is essential reading for the business and investor community.",
            "http://www.afr.com",
            "business",
            "en",
            "au"
        ),
        Sources(
            "argaam",
            "Argaam",
            "ارقام موقع متخصص في متابعة سوق الأسهم السعودي تداول - تاسي - مع تغطيه معمقة لشركات واسعار ومنتجات البتروكيماويات , تقارير مالية الاكتتابات الجديده ",
            "http://www.argaam.com",
            "business",
            "ar",
            "sa"
        ),
        Sources(
            "australian-financial-review",
            "Australian Financial Review",
            "The Australian Financial Review reports the latest news from business, finance, investment and politics, updated in real time. It has a reputation for independent, award-winning journalism and is essential reading for the business and investor community.",
            "http://www.afr.com",
            "business",
            "en",
            "au"
        ),
    )
}