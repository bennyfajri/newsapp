package com.drsync.newsapp.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.drsync.core.data.NewsAppRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repository: NewsAppRepository
) : ViewModel() {
    fun getSources(category: String) = repository.getSource(category).asLiveData()

    fun getTopNews(country: String) = repository.getTopNews(country).cachedIn(viewModelScope).asLiveData()
}