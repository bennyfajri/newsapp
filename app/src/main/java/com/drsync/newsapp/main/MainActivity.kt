package com.drsync.newsapp.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.drsync.core.data.local.Category
import com.drsync.core.data.local.ObjectCategory
import com.drsync.core.ui.CategoryAdapter
import com.drsync.core.ui.LoadingStateAdapter
import com.drsync.core.ui.NewsListAdapter
import com.drsync.core.ui.SourceAdapter
import com.drsync.core.util.ConstantVariable.TAG
import com.drsync.newsapp.databinding.ActivityMainBinding
import com.drsync.newsapp.databinding.FragmentListSourcesBinding
import com.drsync.newsapp.detail.DetailNewsActivity
import com.drsync.newsapp.listnews.ListNewsActivity
import com.drsync.newsapp.listnews.ListNewsActivity.Companion.TAG_SOURCE
import com.google.android.material.bottomsheet.BottomSheetDialog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var mAdapter: CategoryAdapter
    private lateinit var mNewsAdapter: NewsListAdapter
    private lateinit var mSourceAdapter: SourceAdapter

    private var listCategory: ArrayList<Category> = arrayListOf()
    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbarLayout.root)
        setupCategoryRecycler()
        setupAllNewsRecycler()

        binding.apply {
            toolbarLayout.also {
                it.etSearch.isFocusable = false
                it.ilSearch.isFocusable = false
                it.etSearch.inputType = InputType.TYPE_NULL
                it.etSearch.setOnClickListener {
                    Intent(this@MainActivity, ListNewsActivity::class.java).also { intent ->
                        startActivity(intent)
                    }
                }
            }
        }
    }

    private fun setupAllNewsRecycler() {
        viewModel.getTopNews("id").observe(this@MainActivity) {
            mNewsAdapter.submitData(lifecycle, it)
        }

        mNewsAdapter = NewsListAdapter { url ->
            goToNewsDetail(url)
        }
        binding.rvAllNews.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            setHasFixedSize(true)
            adapter = mNewsAdapter.withLoadStateFooter(
                footer = LoadingStateAdapter {
                    mNewsAdapter.retry()
                }
            )
        }
    }

    private fun goToNewsDetail(url: String) {
        Intent(this@MainActivity, DetailNewsActivity::class.java).also {
            it.putExtra(DetailNewsActivity.TAG_URL, url)
            startActivity(it)
        }
    }

    private fun showSourceBottomSheet(category: String) {
        val dialog = BottomSheetDialog(this)
        val binding1 = FragmentListSourcesBinding.inflate(layoutInflater)
        dialog.setContentView(binding1.root)

        mSourceAdapter = SourceAdapter {
            toListNewsActivity(it)
        }
        viewModel.getSources(category).observe(this@MainActivity) { result ->
            when (result) {
                is com.drsync.core.data.Result.Loading -> {
                    binding1.progressBar.isVisible = true
                }
                is com.drsync.core.data.Result.Success -> {
                    binding1.progressBar.isVisible = false
                    setupSourceRecycler(binding1)
                    mSourceAdapter.submitList(result.data.sources)
                }
                is com.drsync.core.data.Result.Error -> {
                    binding1.progressBar.isVisible = false
                    Log.d(TAG, "showSourceBottomSheet: ${result.error}")
                    Toast.makeText(
                        applicationContext,
                        result.error,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }

        setupSourceRecycler(binding1)
        dialog.show()
    }

    private fun setupCategoryRecycler() {
        listCategory = ObjectCategory.listCategory
        mAdapter = CategoryAdapter {
            showSourceBottomSheet(it)
        }
        mAdapter.submitList(listCategory)
        binding.rvCategory.apply {
            adapter = mAdapter
            layoutManager = GridLayoutManager(this@MainActivity, 4)
            setHasFixedSize(true)
        }
    }

    private fun setupSourceRecycler(binding1: FragmentListSourcesBinding) {
        binding1.rvSources.apply {
            adapter = mSourceAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
            setHasFixedSize(true)
        }
    }

    private fun toListNewsActivity(source: String) {
        Intent(this@MainActivity, ListNewsActivity::class.java).also {
            it.putExtra(TAG_SOURCE, source)
            startActivity(it)
        }
    }
}