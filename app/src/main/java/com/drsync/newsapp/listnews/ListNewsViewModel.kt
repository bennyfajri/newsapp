package com.drsync.newsapp.listnews

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.drsync.core.data.NewsAppRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ListNewsViewModel @Inject constructor(
    private val repository: NewsAppRepository
) : ViewModel() {
    fun getNewsBySource(source: String) = repository.getNewsBySource(source).cachedIn(viewModelScope).asLiveData()

    fun getNews(query: String? = null) = repository.getNews(query).cachedIn(viewModelScope).asLiveData()
}