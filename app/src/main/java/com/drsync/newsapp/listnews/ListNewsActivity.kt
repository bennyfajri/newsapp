package com.drsync.newsapp.listnews

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.drsync.core.ui.LoadingStateAdapter
import com.drsync.core.ui.NewsListAdapter
import com.drsync.newsapp.databinding.ActivityListNewsBinding
import com.drsync.newsapp.detail.DetailNewsActivity
import com.drsync.newsapp.detail.DetailNewsActivity.Companion.TAG_URL
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ListNewsActivity : AppCompatActivity() {

    companion object {
        const val TAG_SOURCE = "news_source"
    }

    private lateinit var binding: ActivityListNewsBinding
    private lateinit var mAdapter: NewsListAdapter
    private val viewModel: ListNewsViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListNewsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbarLayout.root)

        mAdapter = NewsListAdapter { url ->
            goToNewsDetail(url)
        }

        if (intent.hasExtra(TAG_SOURCE)) {
            val source = intent.getStringExtra(TAG_SOURCE).toString()
            getListNews(source)
            setupRecyclerView()
        }

        binding.toolbarLayout.etSearch.doAfterTextChanged {
            lifecycleScope.launch {
                delay(400)
                val query = it.toString()
                if (query.isNotEmpty()) {
                    viewModel.getNews(query).observe(this@ListNewsActivity) { pagingData ->
                        mAdapter.submitData(lifecycle, pagingData)
                        setupRecyclerView()
                    }
                }
            }

        }
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        binding.rvNews.apply {
            layoutManager = LinearLayoutManager(this@ListNewsActivity)
            setHasFixedSize(true)
            adapter = mAdapter.withLoadStateFooter(
                footer = LoadingStateAdapter {
                    mAdapter.retry()
                }
            )
        }
    }

    private fun goToNewsDetail(url: String) {
        Intent(this@ListNewsActivity, DetailNewsActivity::class.java).also {
            it.putExtra(TAG_URL, url)
            startActivity(it)
        }
    }

    private fun getListNews(source: String) {
        viewModel.getNewsBySource(source).observe(this@ListNewsActivity) {
            mAdapter.submitData(lifecycle, it)
        }
    }
}