package com.drsync.newsapp.detail

import android.annotation.SuppressLint
import android.os.Bundle
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.drsync.newsapp.databinding.ActivityDetailNewsBinding

class DetailNewsActivity : AppCompatActivity() {

    companion object {
        const val TAG_URL = "detail_url"
    }

    private lateinit var binding: ActivityDetailNewsBinding

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailNewsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val url = intent.getStringExtra(TAG_URL).toString()
        binding.webView.apply {
            loadUrl(url)
            webViewClient = WebViewClient()
            settings.javaScriptEnabled = true
        }
    }
}